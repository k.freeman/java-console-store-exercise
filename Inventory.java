/* Class that sort of faux extends the HashMap class. Used to hold items and keep track of
*  how many the user or store has. Can add and subtract items from the HashMap and has a 
*  CheckForItem helper method to simplify searching the table for an item without an item instance.
*/

import java.util.HashMap;

public class Inventory {
    public HashMap<Item,Integer> itemHashMap;

    public Inventory(){
        itemHashMap = new HashMap<Item,Integer>();
    }

    public Inventory(Item[] itemList){
        itemHashMap = new HashMap<Item,Integer>();
        for (Item item : itemList) {
            itemHashMap.put(item, 10);
        }
    }

    // Check that the item HashMap contains the item and it has more than 0 of them
    // return that item if it does, null if not. 
    public Item CheckForItem(String itemName){
        for (Item item : itemHashMap.keySet()) {
            if(item.GetName().equals(itemName) && itemHashMap.get(item) > 0)
                return item;
        }
        return null;
    }

    public void AddItem(Item item) {
        if (itemHashMap.keySet().contains(item)){
            itemHashMap.put(item, itemHashMap.get(item) + 1);
        }
        else {
            itemHashMap.put(item, 1);
        }
    }

    public void Additem(Item item, int amount){
        if (itemHashMap.keySet().contains(item)){
            itemHashMap.put(item, itemHashMap.get(item) + amount);
        }
        else {
            itemHashMap.put(item, amount);
        }
    }

    public void SubtractItem(Item item){
        if (itemHashMap.keySet().contains(item) && itemHashMap.get(item) > 0){
            itemHashMap.put(item, itemHashMap.get(item) - 1);
            return;
        }
        System.out.println("Cannot subtract a(n) " + item.GetName() + " already at 0.");
    }

    public void SubtractItem(Item item, int amount){
        if (itemHashMap.keySet().contains(item) && itemHashMap.get(item) > 0){
            amount = Math.max(0, itemHashMap.get(item) - amount);
            itemHashMap.put(item, amount);
            return;
        }
        System.out.println("Cannot subtract a(n) " + item.GetName() + " already at 0.");
    }
}    