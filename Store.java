/* Fake store program. Made for an excercise and refresher using Java.
 * May add more functionality in the future, and attempt to add a gui
 * using JavaFX if I have time.
 */
import java.util.Scanner;

public class Store {

    public static Patron currentPatron;
    public static Inventory inventory;
    private static boolean exit = false;

    public static void main (String [] args) {
        currentPatron = new Patron("Bob", 100.0);
        InitInventory();
        System.out.println("Welcome to the store!");
        System.out.println("What do you want to do? Type 'commands' for a list of commands");
        Scanner sc = new Scanner(System.in);
        do{
            System.out.print("> ");
            ParseInput(sc);
        } while (!exit);
        sc.close();
    }

    // Interpret the input from the command line by token.
    // Just splits the input string by white space and calls functions through 
    // a switch case system. 
    public static void ParseInput(Scanner sc) {
        String [] input = sc.nextLine().toLowerCase().split("\\s");
        switch(input[0]){
            case "buy":
                try {
                    Item item = inventory.CheckForItem(input[1]);
                    if(item != null){
                        if(input.length > 2)
                            BuyItem(item, input[2]);
                        else 
                            BuyItem(item, "1");
                    }
                    else {
                        System.out.println("Either '" + input[1] + "' is not in stock or it's not an item we're selling, sorry!");
                    }
                } catch (ArrayIndexOutOfBoundsException exception) {
                    System.out.println("input did not specify an item - (IndexOutOfBoundsException)");
                }
                break;
            case "sell":
                try {
                    Item item = currentPatron.inventory.CheckForItem(input[1]);
                    if(item != null)
                        if(input.length > 2)
                            SellItem(item, input[2]);
                        else
                            SellItem(item, "1");
                    else 
                        System.out.println("You don't have a(n) '" + input[1] + "' to sell, or it's not an available item to sell.");
                } catch (ArrayIndexOutOfBoundsException exception) {
                    System.out.println("input did not specify an item - (IndexOutOfBoundsException)");
                }
                break;
            case "inv":
                currentPatron.ListInventory();
                break;
            case "stock":
                ListStock();
                break;
            case "commands":
                ListCommands();
                break;
            case "exit":
                Exit();
                break;
            case "money":
                System.out.println("You have $" + currentPatron.GetTotalMoney());
                break;
            default:
                System.out.println(input[0] + " is not an available command, type 'commands' for a list of currently available commands");
                break;
        }
    }

    // Print out all commands that are currently available to use.
    public static void ListCommands() {
        System.out.println("Current commands are:");
        System.out.println("buy 'item' -- Buys 'item' and store it in the patron's inventory.");
        System.out.println("sell 'item' -- Sells 'item' back to the store if the patron currently owns said item.");
        System.out.println("inv -- Lists the patrons current inventory.");
        System.out.println("stock -- Lists the store's current stock.");
        System.out.println("money -- display your current money total");
        System.out.println("exit -- Exit the program");
    }

    // Buys 'amount' of item from the store for the patron if the patron can afford them.
    // Otherwise, returns without buying anything.
    public static void BuyItem(Item item, String amount) {
        try {
            int val = Integer.parseInt(amount);
            if (currentPatron.GetTotalMoney() >= item.GetCost() * val){
                currentPatron.inventory.Additem(item, val);
                currentPatron.totalMoney -= item.GetCost() * val;
            }
            else {
                System.out.println("You can't afford " + val + " " + item.GetName() + "!");
                System.out.println(val + " " + item.GetName() + " cost: $" + item.GetCost() * val);
                System.out.println("Current total money: " + currentPatron.GetTotalMoney());
            }
        } catch (NumberFormatException e) {
            System.out.println(amount + " is not a valid argument (either is not numerical or not an integer)");
            System.out.println("buy 'item' '#' is the proper format for the command!");
        }
    }

    // Sell the specified item from the patron's inventory back to the store.
    // Will only sell the amount specified if the patron owns that amount or more.
    // Otherwise, will sell the current amount owned by the patron.
    public static void SellItem(Item item, String amount) {
        try {
            int val = Integer.parseInt(amount);
            int patronAmount = currentPatron.inventory.itemHashMap.get(item);
            if (patronAmount < val){
                System.out.println("Can only sell" + patronAmount + " " + item.GetName());
                val = patronAmount;
            }
            currentPatron.inventory.SubtractItem(item, val);
            inventory.Additem(item, val);
            currentPatron.totalMoney += item.GetCost() * val;
            System.out.println("Sold " + val + " " + item.GetName() + " for $" + item.GetCost() * val + ".");
            System.out.println("Current Inventory:");
            currentPatron.ListInventory();
        } catch (NumberFormatException e) {
            System.out.println(amount + " is not a valid argument (either is not numerical or not an integer)");
            System.out.println("sell 'item' '#' is the proper format for the command!");
        }
    }

    // Exit the main loop, closing the program
    public static void Exit(){
        System.out.println("Exiting");
        if (!exit) exit = true;
    }

    // Check current stock for the item requested.
    public static boolean CheckForItem(String itemName){
        for (Item item : inventory.itemHashMap.keySet()) {
            System.out.println(item.GetName() + " = " + itemName + "?");
            if(item.GetName().equals(itemName) && inventory.itemHashMap.get(item) != 0){
                return true;
            }
        }
        return false;
    }

    // List the current stock of items available from the stores inventory.
    public static void ListStock(){
        System.out.println("Current Stock:");
        if (inventory.itemHashMap.isEmpty()){
            System.out.println("NONE");
            return;
        }
        for (Item item : inventory.itemHashMap.keySet()) {
            System.out.println(item.GetName() + " --- $" + item.GetCost() + " --- " + inventory.itemHashMap.get(item) + " in stock");
        }
        System.out.println("-----------------");
    }

    // Initialize the Store's inventory with some items.
    public static void InitInventory(){
        Item[] itemList = new Item[10];
        String[] names = new String[] 
            {"apple", "banana", 
            "potato", "pear", 
            "strawberries", "lettuce", 
            "carrots", "celery",
            "onion", "mango"};
        double[] prices = new double[] {0.99, 1.99, 1.99, 2.99, 0.50, 2.49, 3.49, 1.75, 2.24, 4.99};
        for (int i = 0; i < 10; i++) {
            itemList[i] = new Item(names[i], prices[i]);
        }
        inventory = new Inventory(itemList);
    }
}

