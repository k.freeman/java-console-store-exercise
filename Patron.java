/* Class used to represent the user of the program 
 * refered to as the 'patron'. Holds a name, money total,
 * and an Inventory instance to keep track of items bought.
 * Has a few helper methods such as ListInventory and CheckForItem
 */

public class Patron{
    private String name;
    public double totalMoney;
    public Inventory inventory;

    public Patron(String patronName, double money){
        name = patronName;
        totalMoney = money;
        inventory = new Inventory();
    }

    public String GetName(){
        return name;
    }

    public double GetTotalMoney(){
        return totalMoney;
    }

    public void ListInventory(){
        System.out.println(inventory.itemHashMap);
    }

    public boolean CheckForItem(String itemName){
        return inventory.CheckForItem(itemName) != null;
    }
}