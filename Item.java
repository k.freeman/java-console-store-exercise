/* Small class that represents an 'item'
 * Holds a String for the name of item and a double for the cose of the item, 
 * as well as a method UpdateCost to change the cost of the item. 
 */
public class Item {
    
    private String name;
    private double cost;

    public static void main(String [] args){
        System.out.println("The Item class");
    }
    
    public Item(String name, double cost){
        this.name = name;
        this.cost = cost;
    }

    public String GetName() {
        return name;
    }

    public double GetCost() {
        return cost;
    }

    // Change the cost of the item.
    public void UpdateCost(double newCost) {
        if (newCost < 0) {
            System.out.println("New cost cannot be less than zero, retaining current cost.");
            return;
        }
        this.cost = newCost;
    }

    public String toString(){
        return "(" + name + ", " + cost + ")";
    }
    
}