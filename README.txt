ver 1.0
---- TO RUN --- 
Compile all .java files and then execute
java Store

This is a basic console application that tries to simulate the interaction between a customer (called a patron) object and a store object.
Both the store and the patron objects store a reference to an Inventory object that contains a HashMap collection of Items. 
Item objects hold a String for their name and a double for their price. 

The patron can buy from and sell to the store. Items that are bought are stored in the patrons inventory and when sold are removed from the 
inventory.

Currently, the store's inventory displays the current "stock" of each item, which will increase when the patron sells back to the store, but 
does not decrease when the patron buys from the store. 

Inputing 'commands' will return a list of available commands. 